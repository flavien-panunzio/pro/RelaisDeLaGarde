<div class="elevage containter-fluid">
	<div class="navigation">
		<div class="titre">
			<h2><?= $site->elevage_h2 ?></h2>
			<?php include('images/separateur_blanc.svg'); ?>
		</div>
		<div class="filtres container no-padding">
			<div class="row no-padding">
				<div class="filtre col-md-3 no-padding active" id="all">
					<p><?= $site->elevage_nav1 ?></p>
				</div>
				<div class="filtre col-md-3 no-padding" id="pouliniere">
					<p><?= $site->elevage_nav3 ?></p>
				</div>
				<div class="filtre col-md-3 no-padding" id="etalon">
					<p><?= $site->elevage_nav4 ?></p>
				</div>
				<div class="filtre col-md-3 no-padding" id="vendus">
					<p><?= $site->elevage_nav5 ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="mosaique row no-padding">
		<?php foreach ($elevage as $key => $value) :
			$icon = '';
		?>
			<div class="col-sm-4 no-padding case <?= $value->type ?>">
				<img src="<?= $value->image ?>" alt="<?= $value->name ?>">
				<div class="overlay">
					<div>
						<h3><?= $value->name ?></h3>
						<p><u><?=  $site->{$types[$value->type]} ?></u></p>
					</div>

					<p><?= $value->description ?></p>
					<a class="btn btn-style" href="/elevage-show/<?= $value->id ?>"><?= $site->plus ?></a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="apropos">
		<div class="titre">
			<h2><?= $site->apropos ?></h2>
			<?php include('images/separateur_blanc.svg'); ?>
		</div>
		<p><?= $site->txtpropos ?></p>
	</div>
</div>