<div class="elevage-show containter-fluid">
	<div class="row  no-padding">
		<div class="mosaique col-sm-4 no-padding">
			<div class="grd-img">
				<img class="plus" src="<?=$show->image?>" alt="Photo de <?=$show->name;?> 1">
			</div>
		</div>
		<div class="miniatures col-sm-2 no-padding">
			<div class="pti-image row no-padding">
				<div class="div-img no-padding">
					<img class="plus" src="<?=$show->image1?>" alt="Photo de <?=$show->name;?> 2">
				</div>
				<div class="div-img no-padding">
					<img class="plus" src="<?=$show->image2?>" alt="Photo de <?=$show->name;?> 3">
				</div>
				<div class="div-img no-padding">
					<img class="plus" src="<?=$show->image3?>" alt="Photo de <?=$show->name;?> 4">
				</div>
			</div>
		</div>
		<div class="description col-sm-6">
			<div class="tableau no-padding">
				<h2><?=$show->name;?></h2>
				<p><?= $site->{$types[$show->type]};?></p>
				<?php include('images/separateur_blanc.svg'); ?>	
				<p><?=$show->description;?></p>
				<?php include('images/separateur_blanc.svg'); ?>
				<div class="grille no-padding">
					<table class="table table-sm table-hover">
						<tr>
							<th><?=$site->elevage_desc1?></th>
							<td><?= $site->{$types[$show->type]};?></td>
						</tr>
						<tr>
							<th><?=$site->elevage_desc2?></th>
							<td><?= $show->race;?></td>
						</tr>
						<tr>
							<th><?=$site->elevage_desc3?></th>
							<td><?= date("d-m-Y",strtotime($show->age))?></td>
						</tr>
						<?php if ($show->taille != 0 && $show->taille !='' && $show->taille != null) : ?>
							<tr>
								<th><?=$site->elevage_desc4?></th>
								<td><?= $show->taille;?> cm</td>
							</tr>
						<?php endif; ?>
						<?php if ($show->discipline!='' && $show->discipline!= null) : ?>
							<tr>
								<th><?=$site->elevage_desc5?></th>
								<td><?= $show->discipline;?></td>
							</tr>
						<?php endif; ?>
						<?php if ($show->prix != 0 && $show->prix !='' && $show->prix !=null) : ?>
							<tr>
								<th><?=$site->elevage_desc6?></th>
								<td><?= $show->prix;?></td>
							</tr>
						<?php endif; ?>
					</table>
				</div>
			</div>
			<div class="navigation">
				<a class="btn btn-style2"  href="/elevage-show/<?=$prev->id;?>"><?=$site->precedent?></a>
				<a class="btn btn-style2"  href="/elevage-index"><?=$site->fermer?></a>
				<a class="btn btn-style2"  href="/elevage-show/<?=$next->id;?>"><?=$site->suivant?></a>
			</div>
		</div>
	</div>
</div>