<div class="index container-fluid" id="content">
	<div class="row presentation">
		<div class="col-md-6 txt-presentation">
			<h2>Le Relais de la Garde</h2>
			<p>
				<?=$site->index_desc?>
			</p>
		</div>
		<div class="col-md-6 img-presentation d-none d-md-block" title="Le Relais de la Garde">
		</div>
		
	</div>
	<div class="container-fluid no-padding">
		<div class="row">
			<div class="div-img col-md-4 no-padding">
				<img src="/images/chambres/index.jpg" alt="La chambre d'hôtes">
				<div class="overlay">
					<h3><?=$site->nav_2?></h3>
					<p><?=$site->index_desc1?></p>
					<a class="btn btn-style" href="/hebergement-index"><?=$site->plus?></a>
				</div>
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="/images/elevage/index.jpg" alt="Aperçu de nos chevaux : Paint Horse">
				<div class="overlay">
					<h3><?=$site->nav_3?></h3>
					<p><?=$site->index_desc2?></p>
					<a class="btn btn-style" href="/elevage-index"><?=$site->plus?></a>
				</div>
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="/images/activites/index.jpg" alt="Les activites de la région">
				<div class="overlay">
					<h3><?=$site->nav_4?></h3>
					<p><?=$site->index_desc3?></p>
					<a class="btn btn-style" href="/activites-index"><?=$site->plus?></a>
				</div>
			</div>
		</div>
	</div>
</div>