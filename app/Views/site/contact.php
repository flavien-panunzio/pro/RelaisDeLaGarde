<div class="container-fluid contact">
	<div class="row">
		<form class="col-lg-8" method="post">
			<div class="form-row">
				<div class="col">
					<label for="nom"><?=$site->contact_1?></label>
					<input id="nom" type="text" name="nom" class="form-control" required>	
				</div>
				<div class="col">
					<label for="prenom"><?=$site->contact_2?></label>
					<input id="prenom" type="text" name="prenom" class="form-control" required>	
				</div>
			</div>
			<div class="form-group">
				<label for="email"><?=$site->contact_3?></label>
				<input id="email" type="email" name="email" class="form-control" aria-describedby="emailHelp" required>
			</div>
			<div class="form-group">
				<label for="message"><?=$site->contact_4?></label>
				<textarea id="message" class="form-control" cols="3" name="message"></textarea>
			</div>
			<div class="form-group">
				<div class="g-recaptcha" data-sitekey="6Ledt2cUAAAAAOwhVnjuhuNwrSYh7E5CVU9hLyeV"></div>
			</div>
			<button type="submit" class="btn btn-style"><?=$site->contact_5?></button>
		</form>
		<div class="col-lg-4 maps no-padding">
			<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=3.590641021728516%2C45.08285432862354%2C3.6472892761230473%2C45.10975600522707&amp;layer=transportmap&amp;marker=45.09630675111088%2C3.6189651489257812"></iframe>
		</div>
	</div>
</div>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>