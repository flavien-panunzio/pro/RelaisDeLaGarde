<div class="activites-show">
	<div class="d-flex navigation">
		<a class="<?php if($_GET['id']=='gastronomie') echo "actif"; ?>" href="/activites-locale/gastronomie"><?=$site->activite1?></a>
		<a class="<?php if($_GET['id']=='visites') echo "actif"; ?>" href="/activites-locale/visites"><?=$site->activite2?></a>
		<a class="<?php if($_GET['id']=='sport') echo "actif"; ?>" href="/activites-locale/sport"><?=$site->activite3?></a>
	</div>
	<?php $i=0; 
	foreach ($locale as  $value) :  $i++;
		if($i%2 == 1){
			$order1="order-1";
			$order2="order-2";
		}
		else{
			$order1="order-2";
			$order2="order-1";
		}?>
		<div class="d-flex ligne">
			<div class="col-4 activites-images no-padding <?=$order1?>">
				<img src="<?=$value->image?>" alt="<?=$value->titre?>">
			</div>
			<div class="col-8 activites-desc no-padding d-flex <?=$order2?>">
				<h2><?=$value->titre?></h2>
				<p><?=$value->description?></p>
				<?php if ($value->lien!='' && $value->lien!=null) : ?>
					<a class="btn btn-style3" href="<?=$value->lien?>" target="_blank">En Savoir plus</a>
				<?php endif; ?>
			</div>	
		</div>
	<?php endforeach; ?>
</div>