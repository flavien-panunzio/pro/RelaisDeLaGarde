<div class="activites container-fluid no-padding" id="content">
	<div class="presentation no-padding">
		<h2><?=$site->activite_h21?></h2>
		<?php include('images/separateur_blanc.svg'); ?>
		<p><?=$site->activite_desc?></p>
	</div>
	<div class="container-fluid">
		<h2><?=$site->activite_h22?></h2>
		<?php include('images/separateur_blanc.svg'); ?>
		<div class="row">
			<div class="div-img col-md-4 no-padding">
				<img src="/images/activites/gastronomie.jpg" alt="La gastronomie Auvergnate">
				<div class="overlay">
					<h3><?=$site->activite1?></h3>
					<p><?=$site->activite_desc1?></p>
					<a class="btn btn-style" href="/activites-locale/gastronomie"><?=$site->plus?></a>
				</div>
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="/images/activites/visites.jpg" alt="La nature en Auvergne : Le Sancy">
				<div class="overlay">
					<h3><?=$site->activite2?></h3>
					<p><?=$site->activite_desc2?></p>
					<a class="btn btn-style" href="/activites-locale/visites"><?=$site->plus?></a>
				</div>
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="/images/activites/sport.jpg" alt="Le sport en Auvergne : Le rafting">
				<div class="overlay">
					<h3><?=$site->activite3?></h3>
					<p><?=$site->activite_desc3?></p>
					<a class="btn btn-style" href="/activites-locale/sport"><?=$site->plus?></a>
				</div>
			</div>
		</div>
	</div>
</div>



