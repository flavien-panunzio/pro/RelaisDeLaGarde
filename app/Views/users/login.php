<?php if($errors): ?>
	<script type="text/javascript" src="/js/plugins/bootstrap-notify.min.js"></script>
	<script type="text/javascript">
		$.notify({
			title: "Erreur de connexion : ",
			message: "Veuillez réessayer",
			icon: 'fas fa-exclamation-triangle' 
		},{
			type: "danger"
		});
	</script>
<?php endif;?>

<div class="login-box">
	<form class="login-form" method="post">
		<h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Connexion</h3>
		<div class="form-group">
			<label class="control-label">Login</label>
			<input name="username" class="form-control" type="text" placeholder="Login" autofocus>
		</div>
		<div class="form-group">
			<label class="control-label">Mot de Passe</label>
			<input name="password" class="form-control" type="password" placeholder="Mot de passe">
		</div>
		<div class="form-group btn-container">
			<button class="btn btn-primary btn-block submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>Se connecter</button>
		</div>
	</form>
</div>