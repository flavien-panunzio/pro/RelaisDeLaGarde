<!DOCTYPE html>
<html lang="<?=$_SESSION['lang']?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no">
		<?=$indexation;?>

		<!-- COULEUR BAR URL IPHONE ANDROID WINDOWS-PHONE -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#870000" />
		<meta name="theme-color" content="#870000" />
		<meta name="msapplication-navbutton-color" content="#870000">

		<!-- META OG -->
		<meta property="og:title" content="RG le Relais de la Garde" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?= $_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:image" content="https://www.relaisdelagarde.fr/images/landing.jpg" />
		<meta property="og:description" content="<?=$desc;?>" />
		<meta property="og:site_name" content="<?=$titre;?>">
		
		<meta name="description" content="<?=$desc;?>" />
		<meta name="keywords" content="chambres d'hôte, table d'hôte, cheval, élevage, paint horse, hébergement, cavalier, auvergne, haute-loire, siaugues, randonnée, laniac">

		<meta name="author" content="Panunzio Flavien" />
		<meta name="copyright" content="© 2018 Flavien Panunzio" />

		<meta name="title" content="Relais de la Garde" />
		<title><?=$titre;?></title>

		<link rel="icon" href="/images/favicon.png" type="image/x-icon"/>

		<!-- FONT -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

		<!-- CSS -->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="/style/default/css/style.css">

		<!-- FULLCALENDAR -->
		<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css' />
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<meta name="google-site-verification" content="wZWZhuRhMvX6junAjv4w851r7Rfz02Gi6wwNChCcsHQ" />
	</head>
	<body>
		<header class="header-<?=$page;?>" style="background-image:linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(<?=$background;?>); background-position: <?=$background_position;?>" title="Image d'accueil <?=$titre;?>">
			<nav class="navbar fixed-top justify-content-end">
				<button class="menu-btn btn" type="button">
					<i class="fas fa-bars"></i>
				</button>
				<div class="menu-cache">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a class="nav-link" href="/"><?= $site->nav_1;?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/hebergement-index"><?= $site->nav_2;?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/elevage-index"><?= $site->nav_3;?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/activites-index"><?= $site->nav_4;?></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="/posts-contact"><?= $site->nav_5;?></a>
						</li>
						<li class="nav-item lang">
							<a class="nav-link" hreflang="en" href="/posts-trad/fr<?=$_SERVER['REQUEST_URI'];?>">FR</a>
							<a class="nav-link">&nbsp/&nbsp</a>
							<a class="nav-link" hreflang="fr" href="/posts-trad/en<?=$_SERVER['REQUEST_URI'];?>">EN</a>
						</li>
					</ul>
				</div>
			</nav>
			<div class="container-fluid header-title">
				<h1><?=$titre_page;?></h1>
			</div>
			<a href="#content"><img class="scroll" src="/images/scroll.gif"></a>
		</header>

		<main role="main" class="container-fluid no-padding">
				<?= $content; ?>
		</main>

		<footer>
			<div class="title">
				<h2><?= $site->footer;?></h2>
				<?php include('images/separateur.svg'); ?>
			</div>
			<div class="infos">
				<div class="div-footer adresse">
					<p><a href="https://goo.gl/maps/ypCD2iXBKDq" target="_balnc">Laniac, 43300 <strong>Siaugues Sainte Marie</strong></a></p>
					<p><a href="mailto:contact@relaisdelagarde.fr">contact@relaisdelagarde.fr</a></p>
					<p><a href="tel:0626191558">06.26.19.15.58</a></p>
				</div>
				<div class="div-footer reseaux">
					<a href="http://facebook.com/relaisdelagarde" target="_blank"><p><img src="/images/facebook.svg"> RG Le Relais de la Garde</p></a>
					<a href="https://www.instagram.com" target="_blank"><p><img src="/images/instagram.svg"> @RelaisdelaGarde</p></a>
				</div>
			</div>
			<div class="bandeau">
				<p class="item-bandeau"><a href="/posts-mentionsLegales"><?= $site->footer_1;?></a></p>
				<p class="item-bandeau"><?= $site->footer_2;?></p>
				<p class="item-bandeau"><a href="/posts-contact"><?= $site->footer_3;?></a></p>
			</div>
		</footer>
		<!-- COOKIES -->
		<script type="text/javascript" id="cookieinfo"
			src="//cookieinfoscript.com/js/cookieinfo.min.js"
			data-message ="<?=$site->cookies?>"
			data-linkmsg ="<?=$site->cookies2?>">
		</script>
	</body>
	<!-- JAVASCRIPT -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
	
	<script src='/js/plugins/moment.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/fr.js"></script>
	
	<script type="text/javascript" src="/js/script.js"></script>
</html>