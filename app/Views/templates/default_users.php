<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Main CSS-->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/style/defaultadmin/css/main.css">
		<!-- Font-icon css-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
		<title>Relais de la Garde - Admin</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	</head>
	<body>
		<section class="material-half-bg">
			<div class="cover"></div>
		</section>
		<section class="login-content">
			<div class="logo">
				<h1>Relais de la Garde</h1>
			</div>
			<?= $content; ?>
		</section>

		<!-- The javascript plugin to display page loading on top-->
		<script src="js/plugins/pace.min.js"></script>

		<!-- Essential javascripts for application to work-->
		
		<script src="/js/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<script src="/js/main.js"></script>

		<!-- Page specific javascripts-->
		<script type="text/javascript" src="/js/plugins/chart.js"></script>
		<script type="text/javascript" src="/js/plugins/jquery.vmap.min.js"></script>
		<script type="text/javascript" src="/js/plugins/jquery.vmap.world.js"></script>
		<script type="text/javascript" src="/js/plugins/jquery.vmap.sampledata.js"></script>
		<script type="text/javascript" src="/js/plugins/pace.min.js"></script>
		
	</body>
</html>