<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">

		<title>ADMIN - <?= App::getInstance()->title;?></title>	
		<meta name="title" content="Relais de la Garde" />
		<link rel="icon" href="/images/favicon.png" type="image/x-icon"/>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<meta name="author" content="Panunzio Flavien" />
		<meta name="copyright" content="© 2018 Flavien Panunzio" />

		<link rel="icon" type="icon" href="/public/uploads/favicon-lock.ico" />

		<!-- CSS-->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<link rel="stylesheet" href="/style/defaultadmin/css/chartjs-visualizations.css">
		<link rel="stylesheet" type="text/css" href="/style/defaultadmin/css/main.css">

		<!-- Font-icon css-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">	
	</head>
	<body class="app sidebar-mini rtl">
	 <!-- Navbar-->
		<header class="app-header"><a class="app-header__logo" href="/admin-posts-index"><?= App::getInstance()->title;?></a>
			<!-- Sidebar toggle button--><a href="#" data-toggle="sidebar" aria-label="Hide Sidebar"><i class="fas fa-bars" style="color: white; font-size: 3vh; padding: 1vh;"></i></a>
			<!-- Navbar Right Menu-->
			<ul class="app-nav">				
				<a class="app-nav__item" href="/"><i class="fas fa-sign-out-alt fa-lg"></i></a>
			</ul>
		</header>
	 <!-- Sidebar menu-->
		<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
		<aside class="app-sidebar">
			<ul class="app-menu">
				<li><a class="app-menu__item" href="/admin-posts-index"><i class="app-menu__icon fas fa-tachometer-alt"></i><span class="app-menu__label">Tableau de bord</span></a></li>
				<li><a class="app-menu__item" href="/admin-posts-analytics"><i class="app-menu__icon fas fa-chart-line"></i><span class="app-menu__label">Google Analytics</span></a></li>
				<li><a class="app-menu__item" href="/admin-elevage-index"><i class="app-menu__icon fas fa-chess-knight"></i><span class="app-menu__label">Mes Chevaux</span></a></li>
				<li><a class="app-menu__item" href="/admin-activites-index"><i class="app-menu__icon fas fa-walking"></i><span class="app-menu__label">Les activités</span></a></li>
				<li><a class="app-menu__item" href="/admin-calendar-index"><i class="app-menu__icon fas fa-calendar-alt"></i><span class="app-menu__label">Reservations</span></a></li>
				<li><a class="app-menu__item" href="/admin-media-index"><i class="app-menu__icon fas fa-image"></i><span class="app-menu__label">Mes médias</span></a></li>
				</li>
			</ul>
		</aside>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<main class="app-content">
			<?= $content; ?>
		</main>
		<!-- Essential javascripts for application to work-->
		
		<script src="/js/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<script src="/js/main.js"></script>

		<!-- Page specific javascripts-->
		<script type="text/javascript" src="/js/plugins/sweetalert.min.js"></script>
		<script type="text/javascript" src="/js/plugins/jquery.vmap.min.js"></script>
		<script type="text/javascript" src="/js/plugins/jquery.vmap.world.js"></script>
		<script type="text/javascript" src="/js/plugins/jquery.vmap.sampledata.js"></script>
		<script type="text/javascript" src="/js/plugins/pace.min.js"></script>
	</body>
</html>