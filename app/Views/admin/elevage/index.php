<div class="app-title navigation">
	<div class="tile-title-w-btn">
		<div>
			<h3 class="title">Mes Chevaux</h3>
			<b>Ici vous pouvez ajouter/supprimer/éditer vos chevaux qui sont sur votre site internet</b><br>
			<a href="/elevage-index" target="_blank" class="btn btn-primary">Voir la page</a>
		</div>
		<div class="bouttons">
			<p><a class="btn btn-primary icon-btn" href="/admin-elevage-add"><i class="fa fa-plus"></i>Ajouter un cheval</a></p>
			<div>
				<button class="btn btn-primary btn-navigation1"><i class="fas fa-th-large"></i></button>
				<button class="btn btn-primary btn-navigation2"><i class="fas fa-align-justify"></i></button>	
			</div>
		</div>
	</div>
</div>
<div class="row">
	<?php
	foreach ($elevage as $value) :?>
		<div class="mosaique col-12">
			<div class="tile article">
				<div class="tile-title-w-btn">
					<div class="titreArticle">
						<h3 class="title"><?= $value->name;?></h3>
					</div>
					<div class="btn-group">
						<a class="btn btn-primary" href="/admin-elevage-edit/<?= $value->id ?>"><i class="fa fa-lg fa-edit"></i></a>
						<a class="btn btn-primary" href="/admin-elevage-delete/<?= $value->id ?>"><i class="fa fa-lg fa-trash"></i></a>
						<a class="btn btn-primary" href="/elevage-show/<?= $value->id ?>" target="_blank"><i class="fas fa-external-link-alt"></i></a>
					</div>
				</div>
				<div class="tile-body row">
					<div class="col-2">
						<img src="<?=$value->image?>">
					</div>
					
					<div class="txtarticle col-5">
						<hr>
						<?= $value->description;?>
						<hr>
					</div>
					<table class="table table-sm table-hover col-5">
						<tr>
							<th>Type</th>
							<td><?= $types[$value->type];?></td>
						</tr>
						<tr>
							<th>Race</th>
							<td><?= $value->race;?></td>
						</tr>
						<tr>
							<th>Age</th>
							<td><?= $value->age;?></td>
						</tr>
						<tr>
							<th>Taille (cm)</th>
							<td><?= $value->taille;?></td>
						</tr>
						<tr>
							<th>Disipline(s)</th>
							<td><?= $value->discipline;?></td>
						</tr>
						<tr>
							<th>Prix</th>
							<td><?= $value->prix;?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>