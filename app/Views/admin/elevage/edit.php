<div class="edit-poney no-padding">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-file-text"></i> Modifier un cheval : <b><?= $poney->name; ?></b></h1>
			<p>Espace Administrateur de votre site web</p>
		</div>
		<?php if ($this->id != 0) : ?>
			<a class="btn btn-primary" href="/elevage-show/<?= $this->id ?>" target="_blank"><i class="fas fa-external-link-alt"></i></a>
		<?php endif; ?>
	</div>
	<div class="tile">
		<form method="post">
			<div class="buttons">
				<a class="btn btn-primary" href="/admin-elevage-index" role="button">Retour</a>
				<button type="submit" class="btn btn-primary">Sauvegarder</button>
			</div>

			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-2"></div>
						<h2 class="col-5">Français</h2>
						<h2 class="col-5">English</h2>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label" for="name">Nom du Cheval</label>
						<div class="col-sm-10">
							<input class="form-control" id="name" type="text" maxlength="254" name="fr_name" value="<?= $poney->name; ?>" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label" for="description">Description</label>
						<div class="col-sm-5">
							<textarea class="form-control" name="fr_description" id="description" rows="5" required><?= $poney->description; ?></textarea>
						</div>
						<div class="col-sm-5">
							<textarea class="form-control" name="en_description" id="description" rows="5" required><?= $poney_en->description; ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="type" class="col-sm-2 col-form-label">Type</label>
						<div class="col-sm-10">
							<select class="form-control" id="type" name="type">
								<?php foreach ($types as $key => $type) : ?>
									<option <?php if ($poney->type == $key)  echo 'selected' ?> value="<?= $key ?>"><?= $type; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="race" class="col-sm-2 col-form-label">Race</label>
						<div class="col-sm-5">
							<input type="text" maxlength="254" class="form-control" id="race" name="fr_race" value="<?= $poney->race; ?>" required>
						</div>
						<div class="col-sm-5">
							<input type="text" maxlength="254" class="form-control" id="race" name="en_race" value="<?= $poney->race; ?>" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="age" class="col-sm-2 col-form-label">Date de naissance</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="age" name="age" value="<?= $poney->age; ?>" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="taille" class="col-sm-2 col-form-label">Taille</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="taille" name="taille" value="<?= $poney->taille; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="discipline" class="col-sm-2 col-form-label">Discipline(s)</label>
						<div class="col-sm-5">
							<input type="text" maxlength="254" class="form-control" id="discipline" name="fr_discipline" value="<?= $poney->discipline; ?>">
						</div>
						<div class="col-sm-5">
							<input type="text" maxlength="254" class="form-control" id="discipline" name="en_discipline" value="<?= $poney->discipline; ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="prix" class="col-sm-2 col-form-label">Prix (0 si non affiché)</label>
						<div class="col-sm-10">
							<input type="number" class="form-control" id="prix" name="prix" value="<?= $poney->prix; ?>">
						</div>
					</div>
				</div>

				<?php for ($i = 0; $i < 4; $i++) :
					if ($i == 0)
						$key = '';
					else
						$key = $i;
				?>
					<div class="col-md-6 col-lg-3 image" id="img<?= $key ?>">
						<img class="img" src="<?= $poney->{'image' . $key}; ?>">
						<div class="overlay">
							<button class="btn btn-primary zoom" type="button" data-toggle="modal" data-target="#Modal1" data-content="<?= $key ?>"><i class="fas fa-exchange-alt"></i></button>
							<button class="btn btn-primary zoom" id="<?= $poney->{'image' . $key}; ?>" type="button" data-toggle="modal" data-target="#Modal2"><i class="fas fa-search-plus"></i></button>
							<button class="btn btn-primary suppr" id="<?= $key ?>" type="button"><i class="fas fa-trash"></i></button>
						</div>
					</div>
				<?php $key++;
				endfor; ?>
			</div>
			<input id="input-img" type="hidden" name="image" value="<?= $poney->image; ?>">
			<input id="input-img1" type="hidden" name="image1" value="<?= $poney->image1; ?>">
			<input id="input-img2" type="hidden" name="image2" value="<?= $poney->image2; ?>">
			<input id="input-img3" type="hidden" name="image3" value="<?= $poney->image3; ?>">
		</form>
		<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="divimgUploads">
							<?php
							foreach ($files as $value) : ?>
								<div class="imgUploads" id="<?= $value; ?>">
									<img src="/images/elevage/<?= $value; ?>">
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<img src="" class="imagepreview" style="width: 100%;">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('.zoom').on('click', function() {
			$('.imagepreview').attr('src', $(this).attr('id'));
		});
		$(document).on('click', function() {
			$('#Modal').modal('hide');
		});
	});

	$(".suppr").click(function(event) {
		key = $(this).attr('id');
		$('#img' + key + ' img').attr('src', '/images/default.jpg');
		$("#input-img" + key).attr('value', '/images/default.jpg');
	});

	$('#Modal1').on('show.bs.modal', function(event) {
		button = $(event.relatedTarget);
		result = button.data('content');
		$(".imgUploads").click(function(event) {
			image = $(this).children().attr('src');
			$("#input-img" + result).attr('value', image);
			$('#img' + result + ' img').attr('src', image);
			$('#Modal1').modal('hide');
		});
	});
</script>