<div class="app-title navigation">
	<div class="tile-title-w-btn">
		<div>
			<h3 class="title">Dashboard</h3>
			<b>C'est ici que vous pouvez ajouter/supprimer/éditer gérer votre site internet</b>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-lg-3">
		<div class="widget-small primary coloured-icon"><i class="icon fas fa-user-clock fa-3x"></i>
			<div class="info">
				<h4>Utilisateurs en direct</h4>
				<p><b>1</b></p>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-3">
		<div class="widget-small info coloured-icon"><i class="icon fa fa-thumbs-up fa-3x"></i>
			<div class="info">
				<h4>Likes Facebook</h4>
				<p><b>47</b></p>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-3">
		<div class="widget-small warning coloured-icon"><i class="icon fa fa-users fa-3x"></i>
			<div class="info">
				<h4>Nombre total de visites</h4>
				<p><b>197</b></p>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-lg-3">
		<div class="widget-small danger coloured-icon"><i class="icon far fa-clock fa-3x"></i>
			<div class="info">
				<h4>Durée session moyenne</h4>
				<p><b>1:55</b></p>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="tile">
			<h3 class="tile-title">Line Chart</h3>
			<div class="embed-responsive embed-responsive-16by9">
				<canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="tile">
			<h3 class="tile-title">Bar Chart</h3>
			<div class="embed-responsive embed-responsive-16by9">
				<canvas class="embed-responsive-item" id="barChartDemo"></canvas>
			</div>
		</div>
	</div>


<script type="text/javascript" src="/js/plugins/chart.js"></script>

<script>
(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>

<script type="text/javascript">
	var data = {
		labels: ["January", "February", "March", "April", "May"],
		datasets: [
			{
				label: "My First dataset",
				fillColor: "rgba(220,220,220,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: [65, 59, 80, 81, 56]
			},
			{
				label: "My Second dataset",
				fillColor: "rgba(151,187,205,0.2)",
				strokeColor: "rgba(151,187,205,1)",
				pointColor: "rgba(151,187,205,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(151,187,205,1)",
				data: [28, 48, 40, 19, 86]
			}
		]
	};
	
	var ctxl = $("#lineChartDemo").get(0).getContext("2d");
	var lineChart = new Chart(ctxl).Line(data);
	
	var ctxb = $("#barChartDemo").get(0).getContext("2d");
	var barChart = new Chart(ctxb).Bar(data);
</script>