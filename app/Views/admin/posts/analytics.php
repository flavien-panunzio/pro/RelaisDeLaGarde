<div class="row">
	<div class="col-md-6">
		<div class="tile">
			<h3 class="tile-title">Cette semaine vs la semaine dernière</h3>
			<figcaption>Nombre de sessions</figcaption>
			<div class="embed-responsive embed-responsive-16by9">
				<figure class="embed-responsive-item" id="chart-1-container"></figure>
				<ol class="Chartjs-legend" id="legend-1-container"></ol>
			</div>
		</div>
	</div>
</div>
<div>
	<div id="embed-api-auth-container"></div>
	<div id="view-selector-container"></div>
	<div id="view-name"></div>
	<div id="active-users-container"></div>
</div>


<script>
(function(w,d,s,g,js,fs){
	g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
	js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
	js.src='https://apis.google.com/js/platform.js';
	fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/plugins/chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/js/plugins/active-users.js"></script>
<script src="/js/plugins/view-selector2.js"></script>


<script>
gapi.analytics.ready(function() {
	gapi.analytics.auth.authorize({
		container: 'embed-api-auth-container',
		clientid: '951564475491-0o9s17kmf9ai51ndgph20go3uk2oqd2t.apps.googleusercontent.com'
	});
	var activeUsers = new gapi.analytics.ext.ActiveUsers({
		container: 'active-users-container',
		pollingInterval: 5
	});
	activeUsers.once('success', function() {
		var element = this.container.firstChild;
		var timeout;

		this.on('change', function(data) {
			var element = this.container.firstChild;
			var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
			element.className += (' ' + animationClass);

			clearTimeout(timeout);
			timeout = setTimeout(function() {
				element.className =
						element.className.replace(/ is-(increasing|decreasing)/g, '');
			}, 3000);
		});
	});
	var viewSelector = new gapi.analytics.ext.ViewSelector2({
		container: 'view-selector-container',
	})
	.execute();
	viewSelector.on('viewChange', function(data) {
		var title = document.getElementById('view-name');
		title.textContent = data.property.name + ' (' + data.view.name + ')';
		activeUsers.set(data).execute();
		renderWeekOverWeekChart(data.ids);
	});


	/**
	 * Draw the a chart.js line chart with data from the specified view that
	 * overlays session data for the current week over session data for the
	 * previous week.
	 */
	function renderWeekOverWeekChart(ids) {

		// Adjust `now` to experiment with different days, for testing only...
		var now = moment(); // .subtract(3, 'day');

		var thisWeek = query({
			'ids': ids,
			'dimensions': 'ga:date,ga:nthDay',
			'metrics': 'ga:sessions',
			'start-date': moment(now).subtract(1, 'day').day(1).format('YYYY-MM-DD'),
			'end-date': moment(now).format('YYYY-MM-DD')
		});

		var lastWeek = query({
			'ids': ids,
			'dimensions': 'ga:date,ga:nthDay',
			'metrics': 'ga:sessions',
			'start-date': moment(now).subtract(1, 'day').day(1).subtract(1, 'week')
					.format('YYYY-MM-DD'),
			'end-date': moment(now).subtract(1, 'day').day(7).subtract(1, 'week')
					.format('YYYY-MM-DD')
		});

		Promise.all([thisWeek, lastWeek]).then(function(results) {

			var data1 = results[0].rows.map(function(row) { return +row[2]; });
			var data2 = results[1].rows.map(function(row) { return +row[2]; });
			var labels = results[1].rows.map(function(row) { return +row[0]; });

			labels = labels.map(function(label) {
				return moment(label, 'YYYYMMDD').format('ddd');
			});

			var data = {
				labels : labels,
				datasets : [
					{
						label: 'La semaine dernière',
						fillColor : 'rgba(220,220,220,0.5)',
						strokeColor : 'rgba(220,220,220,1)',
						pointColor : 'rgba(220,220,220,1)',
						pointStrokeColor : '#fff',
						data : data2
					},
					{
						label: 'Cette semaine',
						fillColor : 'rgba(151,187,205,0.5)',
						strokeColor : 'rgba(151,187,205,1)',
						pointColor : 'rgba(151,187,205,1)',
						pointStrokeColor : '#fff',
						data : data1
					}
				]
			};

			new Chart(makeCanvas('chart-1-container')).Line(data);
			generateLegend('legend-1-container', data.datasets);
		});
	}


	/**
	 * Extend the Embed APIs `gapi.analytics.report.Data` component to
	 * return a promise the is fulfilled with the value returned by the API.
	 * @param {Object} params The request parameters.
	 * @return {Promise} A promise.
	 */
	function query(params) {
		return new Promise(function(resolve, reject) {
			var data = new gapi.analytics.report.Data({query: params});
			data.once('success', function(response) { resolve(response); })
					.once('error', function(response) { reject(response); })
					.execute();
		});
	}


	/**
	 * Create a new canvas inside the specified element. Set it to be the width
	 * and height of its container.
	 * @param {string} id The id attribute of the element to host the canvas.
	 * @return {RenderingContext} The 2D canvas context.
	 */
	function makeCanvas(id) {
		var container = document.getElementById(id);
		var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');

		container.innerHTML = '';
		canvas.width = container.offsetWidth;
		canvas.height = container.offsetHeight;
		container.appendChild(canvas);

		return ctx;
	}


	/**
	 * Create a visual legend inside the specified element based off of a
	 * Chart.js dataset.
	 * @param {string} id The id attribute of the element to host the legend.
	 * @param {Array.<Object>} items A list of labels and colors for the legend.
	 */
	function generateLegend(id, items) {
		var legend = document.getElementById(id);
		legend.innerHTML = items.map(function(item) {
			var color = item.color || item.fillColor;
			var label = item.label;
			return '<li><i style="background:' + color + '"></i>' +
					escapeHtml(label) + '</li>';
		}).join('');
	}


	// Set some global Chart.js defaults.
	Chart.defaults.global.animationSteps = 60;
	Chart.defaults.global.animationEasing = 'easeInOutQuart';
	Chart.defaults.global.responsive = true;
	Chart.defaults.global.maintainAspectRatio = false;


	/**
	 * Escapes a potentially unsafe HTML string.
	 * @param {string} str An string that may contain HTML entities.
	 * @return {string} The HTML-escaped string.
	 */
	function escapeHtml(str) {
		var div = document.createElement('div');
		div.appendChild(document.createTextNode(str));
		return div.innerHTML;
	}

});
</script>