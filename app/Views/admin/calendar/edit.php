<div class="edit-calendar no-padding">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-file-text"></i>Réservations</h1>
			<p>Espace Administrateur de votre site web</p>
		</div>
	</div>
	<div class="tile">
		<form method="post">
			<div class="buttons d-flex justify-content-between">
				<a class="btn btn-primary" href="/admin-calendar-index" role="button">Retour</a>
				<button type="submit" class="btn btn-primary">Sauvegarder</button>
			</div>
			<div class="row">
				<?php if ($tables!=null) : ?>
					<div class="col">
						<label for="table">Nom de la réservation</label>
						<select id="table" name="table" class="form-control">
							<option value="ch1"><?=$this->chambre1?></option>
							<option value="ch2"><?=$this->chambre2?></option>
							<option value="ch3"><?=$this->chambre3?></option>
							<option value="ch4"><?=$this->chambre4?></option>
						</select>
					</div>	
				<?php endif; ?>
				<div class="col">
					<label for="name">Nom de la réservation</label>
					<input class="form-control" id="name" type="text" maxlength="254" name="name" value="<?= $calendar->name;?>">
				</div>
				<div class="col">
					<label for="date_start">Date de début</label>
					<input class="form-control" id="date_start" type="date" maxlength="254" name="date_start" value="<?= $calendar->date_start;?>" required>
				</div>
				<div class="col">
					<label for="date_end">Date de fin</label>
					<input class="form-control" id="date_end" type="date" maxlength="254" name="date_end" value="<?= $calendar->date_end;?>" required>
				</div>	
			</div>
		</form>
	</div>
</div>