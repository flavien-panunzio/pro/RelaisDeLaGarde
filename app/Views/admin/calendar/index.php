<div class="app-title navigation">
	<div class="tile-title-w-btn">
		<div>
			<h3 class="title">Mes Réservations</h3>
			<b>Ici vous pouvez ajouter/supprimer/éditer vos reservations</b><br>
		</div>
		<div class="bouttons">
			<p><a class="btn btn-primary icon-btn" href="/admin-calendar-add"><i class="fa fa-plus"></i>Nouvelle reservation</a></p>
		</div>
	</div>
</div>
<div class="row">
	<div class="calendar col-12">
		<?php for ($i=1; $i <= 4; $i++) : $chambre='chambre'.$i; $link=$i; ?>
			<div class="tile">
				<div class="tile-title-w-btn">
					<div class="titreArticle">
						<h2 class="title"><?= $this->$chambre ?></h2>
					</div>
					<?php if ($link==3) $link=2; if ($link==4) $link=3; ?>
					<a href="/hebergement-chambre/<?= $link ?>" target="_blank" class="btn btn-primary">Voir la page</a>
				</div>
				<table class="table table-hover">
					<thead>
						<th scope="col">#</th>
						<th scope="col">Nom de réservation</th>
						<th scope="col">Date de début</th>
						<th scope="col">Date de fin</th>
						<th scope="col">Édition</th>
					</thead>
					<tbody>
						<?php foreach (${'ch'.$i} as $value) :?>
							<tr>
								<td scope="row"><?= $value->id?></td>
								<td><?= $value->name?></td>
								<td><?=  date("d-m-Y", strtotime($value->date_start))?></td>
								<td><?=  date("d-m-Y", strtotime($value->date_end))?></td>
								<td>
									<div class="btn-group">
										<a class="btn btn-primary" href="/admin-calendar-edit/<?= $value->id?>/Chambre<?=$i?>"><i class="fa fa-lg fa-edit"></i></a>
										<a class="btn btn-primary" href="/admin-calendar-delete/<?= $value->id?>/Chambre<?=$i?>"><i class="fa fa-lg fa-trash"></i></a>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		<?php endfor; ?>
	</div>
</div>