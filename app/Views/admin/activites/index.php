<div class="app-title navigation">
	<div class="tile-title-w-btn">
		<div>
			<h3 class="title">Les Activités</h3>
			<b>Ici vous pouvez ajouter/supprimer/éditer les activités présentes sur le site</b>
		</div>
		<div class="bouttons">
			<p><a class="btn btn-primary icon-btn" href="/admin-activites-add"><i class="fa fa-plus"></i>Nouvelle activité</a></p>
		</div>
	</div>
</div>
<div class="row">
	<div class="calendar col-12">
		<?php for ($i=0; $i < 3; $i++) : ?>
			<div class="tile">
				<div class="tile-title-w-btn">
					<div class="titreArticle">
						<h2 class="title"><?= $categ[$i] ?></h2>
					</div>
					<a href="/activites-locale/<?= $categ[$i] ?>" target="_blank" class="btn btn-primary">Voir la page</a>
				</div>
				<table class="table table-hover">
					<thead>
						<th scope="col">Titre</th>
						<th scope="col">Description</th>
						<th scope="col">Lien</th>
						<th scope="col">Image</th>
						<th scope="col">Édition</th>
					</thead>
					<tbody>
						<?php foreach (${ $categ[$i] } as $value) :?>
							<tr>
								<td scope="row"><b><?= $value->titre?></b></td>
								<td><?= $value->description?></td>
								<td><?= $value->lien ?></td>
								<td><img src="<?= $value->image ?>"></td>
								<td>
									<div class="btn-group">
										<a class="btn btn-primary" href="/admin-activites-edit/<?= $value->id?>"><i class="fa fa-lg fa-edit"></i></a>
										<a class="btn btn-primary" href="/admin-activites-delete/<?= $value->id?>"><i class="fa fa-lg fa-trash"></i></a>
									</div>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		<?php endfor; ?>
	</div>
</div>