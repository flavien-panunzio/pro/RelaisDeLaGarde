<div class="edit-calendar no-padding">
	<div class="app-title">
		<div>
			<h1><i class="fa fa-file-text"></i>Activités</h1>
			<p>Espace Administrateur de votre site web</p>
		</div>
	</div>
	<div class="tile">
		<form method="post">
			<div class="buttons d-flex justify-content-between">
				<a class="btn btn-primary" href="/admin-activites-index" role="button">Retour</a>
				<button type="submit" class="btn btn-primary">Sauvegarder</button>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<div class="row">
						<div class="col">
							<h2 class="text-center">Français</h2>
							<label for="titre">Titre de l'activité</label>
							<input class="form-control" id="titre" type="text" maxlength="254" name="titre" value="<?= $activites->titre;?>" required>
						</div>
						<div class="col">
							<h2 class="text-center">Anglais</h2>
							<label for="titre">Titre de l'activité</label>
							<input class="form-control" id="titre" type="text" maxlength="254" name="titre_en" value="<?= $activites_en->titre;?>" required>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<label for="description">Description</label>
							<textarea name="description" class="form-control" id="description" rows="3" required><?= $activites->description;?></textarea>
						</div>
						<div class="col">
							<label for="description">Description</label>
							<textarea name="description_en" class="form-control" id="description" rows="3" required><?= $activites_en->description;?></textarea>
						</div>
					</div>
					<h2 class="text-center">Général</h2>
					<div class="row">
						<div class="col">
							<label for="categ">Catégorie</label>
							<select id="categ" name="categ" class="form-control">
								<option><?= $activites->categ;?></option>
								<?php if($activites->categ!="Gastronomie") :?>
									<option value="Gastronomie">Gastronomie</option>
								<?php endif; ?>
								<?php if($activites->categ!="Visites") :?>
									<option value="Visites">Visites</option>
								<?php endif; ?>
								<?php if($activites->categ!="Sport") :?>
									<option value="Sport">Sport</option>
								<?php endif; ?>
							</select>
						</div>
						<div class="col">
							<label for="lien">Lien de l'activité</label>
							<input class="form-control" id="lien" type="text" maxlength="254" name="lien" value="<?= $activites->lien;?>">
						</div>
					</div>	
				</div>
				<div class="col-sm-4 image">
					<img class="img" src="<?= $activites->image;?>">
					<div class="overlay">
						<button class="btn btn-primary zoom" type="button" data-toggle="modal" data-target="#Modal1"><i class="fas fa-exchange-alt"></i></button>
						<button class="btn btn-primary zoom" id="<?= $activites->image;?>" type="button" data-toggle="modal" data-target="#Modal2"><i class="fas fa-search-plus"></i></button>
						<button class="btn btn-primary suppr" type="button"><i class="fas fa-trash"></i></button>
					</div>
				</div>
			</div>
			<input id="input-img" type="hidden" name="image" value="<?= $activites->image;?>">
		</form>
		<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div class="divimgUploads">
							<?php
								foreach ($files as $value) :?>
									<div class="imgUploads" id="<?=$value;?>">
										<img src="/images/activites/<?=$value;?>">
									</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<img src="" class="imagepreview" style="width: 100%;" >
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$('.zoom').on('click', function() {
		$('.imagepreview').attr('src', $(this).attr('id'));
	});
	$(document).on('click', function() {
		$('#Modal').modal('hide');	 
	});

	$(".suppr").click(function(event) {
		$('.img').attr('src', '/images/default.jpg');
		$("#input-img").attr('value', '/images/default.jpg');
	});

	$('#Modal1').on('show.bs.modal', function(event) {
		$(".imgUploads").click(function(event) {
			image=$(this).children().attr('src');
			$("#input-img").attr('value', image);
			$('.img').attr('src', image);
			$('#Modal1').modal('hide');
		});
	});

</script>