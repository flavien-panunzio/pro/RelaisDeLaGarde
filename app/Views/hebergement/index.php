<div class="hebergement container-fluid no-padding" id="content">
	<div class="presentation no-padding">
		<h2><?=$site->chambre_h2_1?></h2>
		<?php include('images/separateur_blanc.svg'); ?>
		<p><?=$site->chambre_desc?></p>
	</div>
	<div class="container-fluid">
		<h2><?=$site->chambre_h2_2?></h2>
		<?php include('images/separateur_blanc.svg'); ?>
		<div class="row">
			<div class="div-img col-md-4 no-padding">
				<img src="<?=$index[0]->image?>" alt="<?=$index[0]->name?>">
				<div class="overlay">
					<h3><?=$index[0]->name?></h3>
					<p><?=$index[0]->description?></p>
					<a class="btn btn-style" href="/hebergement-chambre/1"><?=$site->plus?></a>
				</div>
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="<?=$index[1]->image?>" alt="<?=$index[1]->name?>">
				<div class="overlay">
					<h3><?=$index[1]->name?></h3>
					<p><?=$index[1]->description?></p>
					<a class="btn btn-style" href="/hebergement-chambre/2"><?=$site->plus?></a>
				</div>
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="<?=$index[2]->image?>" alt="<?=$index[2]->name?>">
				<div class="overlay">
					<h3><?=$index[2]->name?></h3>
					<p><?=$index[2]->description?></p>
					<a class="btn btn-style" href="/hebergement-chambre/3"><?=$site->plus?></a>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<h2><?=$site->chambre_h2_21?></h2>
		<?php include('images/separateur_blanc.svg'); ?>
		<div class="row">
			<div class="div-img col-md-4 no-padding">
				<img src="/images/chambres/commun-1.jpg" alt="Pièce commun-1">
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="/images/chambres/commun-2.jpg" alt="Pièce commun-2">
			</div>
			<div class="div-img col-md-4 no-padding">
				<img src="/images/chambres/commun-3.jpg" alt="Pièce commun-3">
			</div>
		</div>
	</div>
</div>
