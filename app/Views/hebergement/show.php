<div class="hebergement-show containter-fluid">
	<div class="row	no-padding">
		<div class="mosaique col-md-4 no-padding">
			<div class="grd-img row no-padding">
				<img class="col-12 no-padding plus" src="<?=$show->image;?>" alt="Photos principale <?=$show->name;?>">
			</div>
			<div class="pti-image row no-padding">
				<img class="col-xl-4 no-padding plus" src="<?=$show->image1;?>" alt="Photos supplémentaire <?=$show->name;?>">
				<img class="col-xl-4 no-padding plus" src="<?=$show->image2;?>" alt="Photos supplémentaire <?=$show->name;?>">
				<img class="col-xl-4 no-padding plus" src="<?=$show->image3;?>" alt="Photos supplémentaire <?=$show->name;?>">
			</div>
			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="<?=$show->image;?>" alt="Photos supplémentaire <?=$show->name;?>">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="<?=$show->image1;?>" alt="Photos supplémentaire <?=$show->name;?>">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="<?=$show->image2;?>" alt="Photos supplémentaire <?=$show->name;?>">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="<?=$show->image3;?>" alt="Photos supplémentaire <?=$show->name;?>">
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class="description col-md-8 row no-padding">
			<div class="row d-flex align-items-center">
				<div class="tableau no-padding col-xl-<?=$col;?>">
					<h2><?=$show->name;?></h2>
					<?php include('images/separateur_blanc.svg'); ?>	
					<p><?=$show->description;?></p>
					<?php include('images/separateur_blanc.svg'); ?>
					<div class="grille no-padding">
						<table class="table table-sm table-hover">
							<tr>
								<th>Nombre de lits</th>
								<td><?= $show->nb_lits;?></td>
							</tr>
							<tr>
								<th>Nombre de places</th>
								<td><?= $show->nb_places;?></td>
							</tr>
							<tr>
								<th>Prix</th>
								<td><?= $show->prix;?></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-xl-<?=$col2;?> calendar">
					<input id="input-lang" type="hidden" name="lang" value="<?=$_SESSION['lang']?>">
					<input id="input-id" type="hidden" name="lang" value="<?=$_GET['id']?>">
					<?php if($_GET['id']==1) : ?>
						<div id='calendar'></div>
					<?php endif; ?>
					<?php if($_GET['id']==2) : ?>
						<div class="row">
							<div class="col-xl-6">
								<p class="text-center font-weight-bold no-padding"><u>Chambre 2</u></p>
								<div id='calendar'></div>	
							</div>
							<div class="col-xl-6">
								<p class="text-center font-weight-bold no-padding"><u>Chambre 3</u></p>
								<div id='calendar2'></div>
							</div>
						</div>
						
					<?php endif; ?>
					<?php if($_GET['id']==3) : ?>
						<div id='calendar3'></div>
					<?php endif; ?>
				</div>
			</div>
			<div class="navigation">
				<a class="btn btn-style2"	href="/hebergement-chambre/<?=$prev->id;?>"><?=$site->precedent?></a>
				<a class="btn btn-style2"	href="/hebergement-index"><?=$site->fermer?></a>
				<a class="btn btn-style2"	href="/hebergement-chambre/<?=$next->id;?>"><?=$site->suivant?></a>
			</div>
		</div>
	</div>
</div>