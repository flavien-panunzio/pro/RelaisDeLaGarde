<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use \App;

	class AppController extends Controller{

		protected $template = 'default';

		public function __construct(){
			if(strrpos($_SERVER['REQUEST_URI'], 'admin')!=false) {
	    		$this->template = 'default_admin';
	    	}
	    	elseif(strrpos($_SERVER['REQUEST_URI'], 'users')!=false) {
	    		$this->template = 'default_users';
	    	}
	    	else{
	    		$this->template = 'default';
	    	}
			$this->viewPath = ROOT . '/app/Views/';
		}

		protected function loadModel($model_name){
			$this->$model_name = App::getInstance()->getTable($model_name);
		}

		protected function description($page){
			if ($page=="index") {
				$description="RG Le Relais de la Garde vous accueille en Haute Loire à Laniac dans sa chambre d'hôtes avec sa table de qualité. Nous avons également un élevage familial de Paint Horse.";
			}elseif($page=="hebergement"){
				$description="RG Le Relais de la Garde dispose de plusieurs chambres d'hôte construite dans une ferme rénovée. Une table d'hôte est également proposée avec des produits de qualité cuisinés sur place.";
			}elseif($page=="elevage"){
				$description="RG Le Relais de la Garde produits des chevaux de pure race Paint Horse pour la saillie ou la vente, il y a des poulinières, des étalons, des poulains et des cheveux destinés aux compétitions.";
			}elseif($page=="activites"){
				$description="RG Le Relais de la Garde est situé en plein cœur du Massif Centrale dans le Haut-Allier, une région pleine de ressources et d'activités sportive et culturelles à faire en famille, entre amis ou entre collègues.";
			}elseif($page=="contact"){
				$description="RG Le Relais de la Garde est à votre disposition par téléphone au 06.29.19.15.58, vous pouvez réserver une chambre d'hôte ou avoir des informations sur l'élevage de Paint Horse.";
			}
			return $description;
		}
	}