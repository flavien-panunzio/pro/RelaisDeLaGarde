<?php 
	namespace App\Controller;

	class ActivitesController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Post');
			$this->loadModel('Activites');
			$this->loadModel('Site');
		}

		public function index(){
			$desc=$this->description('activites');
			$site= $this->Site;
			$site->trad('site');
			$site=$site->find(1);
			$this->titre='RG Relais de la Garde | '.$site->nav_4;
			$background='/images/activites/cover.jpg';
			$background_position='50% 75%';
			$activites = $this->Post->all();
			$this->render('activites.index', compact('activites','background','background_position','desc'));
		}

		public function locale(){
			$desc=$this->description('activites');
			$site= $this->Site;
			$site->trad('site');
			$site=$site->find(1);
			$this->titre='RG Relais de la Garde | '.$site->nav_4;
			$background='/images/activites/index.jpg';
			$background_position='50% 50%';

			$locale = $this->Activites;
			$locale->trad("activites");
			if($locale=$locale->findWithCategory($_GET['id']))
				$this->render('activites.show', compact('background','background_position','locale','desc'));
			else{
				header('location: /activites-index');
				die;
			}
		}
	}