<?php 
	namespace App\Controller;
	use \App;

	class HebergementController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Chambres');
			$this->loadModel('Chambre1');
			$this->loadModel('Chambre2');
			$this->loadModel('Chambre3');
			$this->loadModel('Chambre4');
			$this->loadModel('Site');
		}

		public function index(){
			$desc=$this->description('hebergement');
			$site= $this->Site;
			$site->trad('site');
			$site=$site->find(1);
			$this->titre='RG Relais de la Garde | '.$site->nav_2;
			$background='/images/chambres/cover.jpg';
			$background_position='50% 55%';
			$index = $this->Chambres;
			$index->trad("chambres");
			$index=$index->all();
			$this->render('hebergement.index', compact('index','background','background_position','desc'));
		}

		public function chambre(){
			$desc=$this->description('hebergement');
			$site= $this->Site;
			$site->trad('site');
			$site=$site->find(1);
			$this->titre='RG Relais de la Garde | '.$site->nav_2;
			$show = $this->Chambres;
			$show->trad("chambres");
			$background='/images/chambres/index.jpg';
			$background_position='50% 55%';
			$next = $show->nextid($_GET['id']);
			$prev = $show->previd($_GET['id']);
			$col=7; $col2=5;
			if ($_GET['id']==2) {
				$col=12; $col2=12;
			}
			if($show=$show->find($_GET['id']))
				$this->render('hebergement.show', compact('show','background','background_position','next','prev','col','col2','desc'));
			else{
				header('location: /hebergement-index');
				die;
			}
		}

		public function json(){
			$date = $this->{"Chambre".$_GET['id']}->all();

			foreach ($date as  $value) {
				 $dates[] = array(
					'start'	 => $value->date_start,
					'end'	 => $value->date_end,
					'rendering'=> 'background'
				 );
			}
			header('Content-Type: application/json');
			echo json_encode($dates);
		}
	}