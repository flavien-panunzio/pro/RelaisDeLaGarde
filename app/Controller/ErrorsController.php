<?php

namespace App\Controller;

use Core\Controller\Controller;

class ErrorsController extends AppController{

	public function __construct(){
		parent::__construct();
		$this->loadModel('Post');
	}

	public function error_404(){
		$indexation='<meta name="robots" content="noindex">';
		$this->titre='RG Relais de la Garde | Erreur 404';
		$this->render('errors.404',compact('indexation'));
	}

	public function error_418(){
		$indexation='<meta name="robots" content="noindex">';
		$this->titre='RG Relais de la Garde | Erreur 418';
		$this->render('errors.418',compact('indexation'));
	}

	public function maintenance(){
		$indexation='<meta name="robots" content="noindex">';
		$this->titre='RG Relais de la Garde | Maintenance';
		$this->render('errors.maintenance',compact('indexation'));
	}

}