<?php 
namespace App\Controller;

class ElevageController extends AppController{
	
	public function __construct(){
		parent::__construct();
		$this->loadModel('Elevage');
		$this->loadModel('Site');
	}
	
	public function index(){
		$desc=$this->description('elevage');
		$site= $this->Site;
		$site->trad('site');
		$site=$site->find(1);
		$this->titre='RG Relais de la Garde | '.$site->nav_3;
		$background='/images/elevage/bg.jpg';
		$background_position='50% 35%';
		$elevage = $this->Elevage;
		$elevage->trad("elevages");
		$elevage = $this->Elevage->all();
		$types = [
			'pouliniere' => "elevage_nav3",
			'etalon' => "elevage_nav4",
			'vendus' => "elevage_nav5",
		];
		$this->render('elevage.index', compact('elevage','background','background_position','desc','types'));
	}
	
	public function show(){
		$desc=$this->description('elevage');
		$site= $this->Site;
		$site->trad('site');
		$site=$site->find(1);
		$this->titre='RG Relais de la Garde | '.$site->nav_3;
		$show = $this->Elevage;
		$show->trad("elevages");
		$background='/images/elevage/bg.jpg';
		$background_position='50% 35%';
		$next = $this->Elevage->nextid($_GET['id']);
		$prev = $this->Elevage->previd($_GET['id']);
		$types = [
			'pouliniere' => "elevage_nav3",
			'etalon' => "elevage_nav4",
			'vendus' => "elevage_nav5",
		];
		if($show = $this->Elevage->find($_GET['id'])){
			$this->render('elevage.show', compact('show','next','prev','background','background_position','desc', 'types'));
		}
		else{
			header('location: /errors-errors-404');
			die;
		}
	}
}