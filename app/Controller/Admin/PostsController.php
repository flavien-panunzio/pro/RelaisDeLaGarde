<?php 
	namespace App\Controller\Admin;

	use \Core\HTML\BootstrapForm;

	class PostsController extends AppController{

		public function __construct(){
			parent::__construct();
			
		}

		public function index(){
			$this->render('admin.posts.index');
		}

		public function analytics(){
			$this->render('admin.posts.analytics');
		}
	}