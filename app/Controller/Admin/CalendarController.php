<?php 
	namespace App\Controller\Admin;

	class CalendarController extends AppController{

		public $chambre1='Chambre 1';
		public $chambre2='Chambre 2';
		public $chambre3='Chambre 3';
		public $chambre4='Chambre 4';

		public function __construct(){
			parent::__construct();
			$this->loadModel('Chambre1');
			$this->loadModel('Chambre2');
			$this->loadModel('Chambre3');
			$this->loadModel('Chambre4');
		}

		public function index(){
			$ch1 = $this->Chambre1->all();
			$ch2 = $this->Chambre2->all();
			$ch3 = $this->Chambre3->all();
			$ch4 = $this->Chambre4->all();
			$this->render('admin.calendar.index', compact('ch1','ch2','ch3','ch4'));
		}

		public function add(){
			$tables=1;
			if (!empty($_POST)) {
				if ($_POST['table']=='ch1') 
					$table='Chambre1';
				elseif ($_POST['table']=='ch2') 
					$table='Chambre2';
				elseif ($_POST['table']=='ch3') 
					$table='Chambre3';
				elseif ($_POST['table']=='ch4') 
					$table='Chambre4';
				$result = $this->$table->create([
					'name' => $_POST['name'],
					'date_start' => $_POST['date_start'],
					'date_end' => $_POST['date_end']
				]);
				if ($result) {
					header('Location: /admin-calendar-index');
					die;
				}
			}
			$calendar=(object)array();
			$calendar->name='';
			$calendar->date_start='';
			$calendar->date_end='';

			$this->render('admin.calendar.edit', compact('calendar','tables'));
		}
		public function edit(){
			$table=explode('/', $_SERVER['REQUEST_URI']);
			$table=str_replace('/', '', $table[3]);
			if ($table!='Chambre1' && $table!='Chambre2' && $table!='Chambre3' && $table!='Chambre4') {
				header('Location: /admin-calendar-index');
				die;
			}
			if (!empty($_POST)) {
				$result = $this->$table->update($_GET['id'], [
					'name' => $_POST['name'],
					'date_start' => $_POST['date_start'],
					'date_end' => $_POST['date_end']
				]);
				if ($result) {
					header('Location: /admin-calendar-index');
					die;
				}
			}
			if($calendar = $this->$table->find($_GET['id'])){
				$tables=null;
				$this->render('admin.calendar.edit', compact('calendar','tables'));	
			}
			else{
				header('Location: /admin-calendar-index');
				die;
			}
		}
		public function delete(){
			$table=explode('/', $_SERVER['REQUEST_URI']);
			$table=str_replace('/', '', $table[3]);
			if ($table!='Chambre1' && $table!='Chambre2' && $table!='Chambre3' && $table!='Chambre4') {
				header('Location: /admin-calendar-index');
				die;
			}
			if (!empty($_GET)) {
				$this->$table->delete($_GET['id']);
			}
			header('Location: /admin-calendar-index');
			die;
		}
	}