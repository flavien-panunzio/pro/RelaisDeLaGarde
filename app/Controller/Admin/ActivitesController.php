<?php 
	namespace App\Controller\Admin;
	use \App;

	class ActivitesController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Activites');
			$this->loadModel('Activites_en');
		}

		public function index(){
			$Gastronomie = $this->Activites->query("SELECT * FROM fr_activites WHERE categ = 'Gastronomie'");
			$Visites = $this->Activites->query("SELECT * FROM fr_activites WHERE categ = 'Visites'");
			$Sport = $this->Activites->query("SELECT * FROM fr_activites WHERE categ = 'Sport'");
			
			$categ=['Gastronomie', 'Visites', 'Sport'];
			$this->render('admin.activites.index', compact('Gastronomie','Visites','Sport','categ'));
		}

		public function add(){
			$tables=1;
			if (!empty($_POST)) {
				$result = $this->Activites->create([
					'titre' => $_POST['titre'],
					'categ' => $_POST['categ'],
					'description' => $_POST['description'],
					'image' => $_POST['image'],
					'lien' => $_POST['lien']
				]);
				$result2 = $this->Activites_en->create([
					'titre' => $_POST['titre_en'],
					'categ' => $_POST['categ'],
					'description' => $_POST['description_en'],
					'image' => $_POST['image'],
					'lien' => $_POST['lien']
				]);
				if ($result && $result2) {
					header('Location: /admin-activites-index');
					die;
				}
			}
			$activites=(object)array();
			$activites->titre='';
			$activites->description='';
			$activites->image='/images/default.jpg';
			$activites->lien='';
			$activites->categ='Gastronomie';
			$activites_en=(object)array();
			$activites_en->description='';

			$this->render('admin.activites.edit', compact('activites','activites_en','tables'));
		}
		public function edit(){
			$files=$this->readDIR('activites');
			if (!empty($_POST)) {
				$result = $this->Activites->update($_GET['id'], [
					'titre' => $_POST['titre'],
					'description' => $_POST['description'],
					'image' => $_POST['image'],
					'categ' => $_POST['categ'],
					'lien' => $_POST['lien']
				]);
				$result2 = $this->Activites_en->update($_GET['id'], [
					'titre' => $_POST['titre_en'],
					'description' => $_POST['description_en'],
					'categ' => $_POST['categ'],
					'image' => $_POST['image'],
					'lien' => $_POST['lien']
				]);
				if ($result && $result2) {
					header('Location: /admin-activites-index');
					die;
				}
			}
			if($activites = $this->Activites->find($_GET['id'])){
				$activites_en = $this->Activites_en->find($_GET['id']);
				$tables=null;
				$this->render('admin.activites.edit', compact('activites','activites_en','tables','files'));	
			}
			else{
				header('Location: /admin-activites-index');
				die;
			}
		}
		public function delete(){
			if (!empty($_GET['id'])) {
				$this->Activites->delete($_GET['id']);
				$this->Activites_en->delete($_GET['id']);
			}
			header('Location: /admin-activites-index');
			die;
		}
	}