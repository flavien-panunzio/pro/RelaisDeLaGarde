<?php 
	namespace App\Controller\Admin;

	use \Core\HTML\BootstrapForm;

	class AnalyticsController extends AppController{

		public function __construct(){
			parent::__construct();
		}

		public function index(){
			$this->render('admin.Analytics.index');
		}
	}