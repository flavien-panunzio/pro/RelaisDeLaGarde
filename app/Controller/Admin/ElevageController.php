<?php 
namespace App\Controller\Admin;

use \Core\HTML\BootstrapForm;

class ElevageController extends AppController{
	
	public $id;
	public function __construct(){
		parent::__construct();
		$this->loadModel('Elevage');
		$this->loadModel('Elevage_en');
		$this->id=0;
	}
	
	public function index(){
		$elevage = $this->Elevage->all();
		$types = [
			'etalon' => "Étalons",
			'pouliniere' => "Poulinières",
			'vendus' => "Chevaux vendus",
		];
		$this->render('admin.elevage.index', compact('elevage', 'types'));
	}
	
	public function add(){
		if (!empty($_POST)) {
			$result1 = $this->Elevage->create([
				'name' => $_POST['fr_name'],
				'description' => $_POST['fr_description'],
				'race' => $_POST['fr_race'],
				'age' => $_POST['age'],
				'taille' => $_POST['taille'],
				'type' => $_POST['type'],
				'discipline' => $_POST['fr_discipline'],
				'prix' => $_POST['prix'],
				'image' => $_POST['image'],
				'image1' => $_POST['image1'],
				'image2' => $_POST['image2'],
				'image3' => $_POST['image3']
			]);
			$result2 = $this->Elevage_en->create([
				'name' => $_POST['fr_name'],
				'description' => $_POST['en_description'],
				'race' => $_POST['en_race'],
				'age' => $_POST['age'],
				'taille' => $_POST['taille'],
				'type' => $_POST['type'],
				'discipline' => $_POST['en_discipline'],
				'prix' => $_POST['prix'],
				'image' => $_POST['image'],
				'image1' => $_POST['image1'],
				'image2' => $_POST['image2'],
				'image3' => $_POST['image3']
			]);
			if ($result1 && $result2) {
				header('Location: /admin-elevage-index');
				die;
			}
		}
		$poney=(object)array();
		$poney_en=(object)array();
		$poney->name='';
		$poney->description='';
		$poney->race='Paint Horse';
		$poney->age=0;
		$poney->taille=0;
		$poney->type="etalon";
		$poney->discipline='';
		$poney->prix=0;
		$poney->image='/images/default.jpg';
		$poney->image1='/images/default.jpg';
		$poney->image2='/images/default.jpg';
		$poney->image3='/images/default.jpg';
		
		$poney_en->name='';
		$poney_en->description='';
		$poney_en->race='Paint Horse';
		$poney_en->discipline='';
		
		$files=$this->readDIR('elevage');
		
		$types = [
			'etalon' => "Étalons",
			'pouliniere' => "Poulinières",
			'vendus' => "Chevaux vendus",
		];
		$this->render('admin.elevage.edit', compact('poney', 'poney_en','types','files'));
	}
	
	public function edit(){
		
		$files=$this->readDIR('elevage');
		
		if (!empty($_POST)) {			
			$result1 = $this->Elevage->update($_GET['id'], [
				'name' => $_POST['fr_name'],
				'description' => $_POST['fr_description'],
				'race' => $_POST['fr_race'],
				'age' => $_POST['age'],
				'taille' => $_POST['taille'],
				'type' => $_POST['type'],
				'discipline' => $_POST['fr_discipline'],
				'prix' => $_POST['prix'],
				'image' => $_POST['image'],
				'image1' => $_POST['image1'],
				'image2' => $_POST['image2'],
				'image3' => $_POST['image3']
			]);
			$result2 = $this->Elevage_en->update($_GET['id'], [
				'name' => $_POST['fr_name'],
				'description' => $_POST['en_description'],
				'race' => $_POST['en_race'],
				'age' => $_POST['age'],
				'taille' => $_POST['taille'],
				'type' => $_POST['type'],
				'discipline' => $_POST['en_discipline'],
				'prix' => $_POST['prix'],
				'image' => $_POST['image'],
				'image1' => $_POST['image1'],
				'image2' => $_POST['image2'],
				'image3' => $_POST['image3']
			]);
			if ($result1 && $result2) {
				header('Location: /admin-elevage-index');
				die;
			}
		}
		$this->id=$_GET['id'];
		$poney = $this->Elevage->find($_GET['id']);
		$poney_en = $this->Elevage_en->find($_GET['id']);
		$types = [
			'etalon' => "Étalons",
			'pouliniere' => "Poulinières",
			'vendus' => "Chevaux vendus",
		];
		$this->render('admin.elevage.edit', compact('poney', 'poney_en', 'types','files'));
	}
	
	public function delete(){
		if (!empty($_GET)) {
			$this->Elevage->delete($_GET['id']);
			$this->Elevage_en->delete($_GET['id']);
		}
		header('Location: /admin-elevage-index');
		die;
	}
}