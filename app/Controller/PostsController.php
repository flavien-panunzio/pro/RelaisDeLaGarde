<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use Core\Table\Table;


	class PostsController extends AppController{

		public function __construct(){
			parent::__construct();
			$this->loadModel('Post');
			$this->loadModel('Chambre1');
			$this->loadModel('Elevage');
		}

		public function trad(){
			$url=explode('/', $_SERVER['REQUEST_URI']);
			$_SESSION['lang']=$url[2];
			if (isset($url[4]))
				header("Location: /".$url[3].'/'.$url[4]);
			else
				header("Location: /".$url[3]);
			die();
		}

		public function index(){
			$desc=$this->description('index');
			$index = $this->Post->all();
			$this->render('site.index', compact('index','desc'));
		}

		public function contact(){
			$desc=$this->description('contact');
			if(isset($_POST) && !empty($_POST)):
				if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
					$secret = '6Ledt2cUAAAAAKo_P0CrKe5ap_sk5iqKNVuEaibj';
					$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
					$responseData = json_decode($verifyResponse);
					if($responseData->success):
						$nom = !empty($_POST['nom'])?$_POST['nom']:'';
						$prenom = !empty($_POST['prenom'])?$_POST['prenom']:'';
						$email = !empty($_POST['email'])?$_POST['email']:'';
						$message = !empty($_POST['message'])?$_POST['message']:'';
						
						$to = 'contact@relaisdelagarde.fr';
						$subject = 'Formulaire de contact relaisdelagarde.fr';
						$htmlContent = "
							<h1>Message du formulaire de contact</h1>
							<p><b>Nom: </b>".$nom."</p>
							<p><b>Préom: </b>".$prenom."</p>
							<p><b>Email: </b>".$email."</p>
							<p><b>Message: </b>".$message."</p>
						";
						$headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers .= 'From:'.$nom.' <'.$email.'>' . "\r\n";
						@mail($to,$subject,$htmlContent,$headers);
						$succMsg = 'Your contact request have submitted successfully.';
					else:
						$errMsg = 'Robot verification failed, please try again.';
					endif;
				else:
					$errMsg = 'Please click on the reCAPTCHA box.';
				endif;
			else:
				$errMsg = '';
				$succMsg = '';
			endif;
			$this->titre='RG Relais de la Garde | Contact';
			$contact = $this->Post->all();
			$this->render('site.contact', compact('contact','desc'));
		}

		public function mentionsLegales(){
			$indexation='<meta name="robots" content="noindex">';
			$this->titre='RG Relais de la Garde | Mentions Légales';
			$this->render('site.mentions', compact('indexation'));
		}
	}