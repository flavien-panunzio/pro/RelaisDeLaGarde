<?php 
	namespace App\Table;
	use Core\Table\Table;
	class PostTable extends Table {

		protected $table = 'fr_elevages';
		
		public function last(){
			return $this->query("
				SELECT *, categories.titre as categorie
				FROM articles 
				LEFT JOIN categories ON category_id = categories.id
				ORDER BY articles.date DESC");
		}

		public function findWithCategory($id){
			return $this->query("
				SELECT articles.id, articles.titre, articles.contenu, articles.date, categories.titre as categorie
				FROM articles 
				LEFT JOIN categories ON category_id = categories.id
				WHERE articles.id = ?", [$id], true);
		}

		public function lastByCategory($category_id){
			return $this->query("
				SELECT articles.id, articles.titre, articles.contenu, categories.titre as categorie
				FROM articles 
				LEFT JOIN categories ON category_id = categories.id
				WHERE articles.category_id = ?
				ORDER BY articles.date DESC", [$category_id]);
		}

	}