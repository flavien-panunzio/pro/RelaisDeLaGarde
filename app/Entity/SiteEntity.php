<?php
	namespace App\Entity;
	use Core\Entity\Entity;

	class SiteEntity extends Entity{

		public function getUrl(){
			return '/posts.template/' . $this->id;
		}
	}