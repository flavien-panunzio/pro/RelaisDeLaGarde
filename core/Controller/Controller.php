<?php

	namespace Core\Controller;
	use \App;
	class Controller{

		protected $viewPath;
		protected $site;
		protected $titre='';

		protected function render($view, $variables = []){
			$site = App::getInstance()->getTable('Site');
			$site->trad('site');
			$site = $site->find(1);
			$indexation='';
			$desc=$this->description('index');
			$titre=explode('-',$_SERVER['REQUEST_URI']);
			if (isset($titre[1]) && ($titre[1]!='index' || ($titre[0]!='/' && $titre[0]!='/posts'))) {
				$titre=$this->titre;
				$page='default';
			}
			else{
				$page='landing';
				$titre='RG Relais de la Garde';
			}
			$titre_page=str_replace('RG Relais de la Garde | ', '', $titre);
			$background='/images/landing.jpg';
			$background_position='50% 50%';
			ob_start();
			extract($variables);
			require ($this->viewPath . str_replace('.','/',$view) . '.php');
			$content = ob_get_clean();

			require ($this->viewPath. 'templates/' . $this->template . '.php');
		}


		protected static function forbidden(){
			header("HTTP/1.0 403 Forbidden");
			header('Location: /users-login');
			die('Accès interdit');
		}

		protected function loadModel($model_name){
			$this->$model_name = App::getInstance()->getTable($model_name);
		}
	}