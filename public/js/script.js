$(document).ready(function(){
	//SMOOTH SCROLL ----------------------------------------------------------------
		$('header a[href^="#"]').click(function(){
			var the_id = $(this).attr("href");
			if (the_id === '#') {
				return;
			}
			$('html, body').animate({
				scrollTop:$(the_id).offset().top
			}, 'slow');
			return false;
		});

	// MENU -----------------------------------------------------------------------
		//TOGGLE MENU
			$('.menu-btn').click(function() {
				$('.menu-cache').toggle('slide');
				$('.navbar').toggleClass('bg-rouge');
			});

		//REDUIRE MENU SCROLL
			var scroll =0;
			$( window ).scroll(function() {
				scroll ++;
				if(scroll >= 35) {
					if ($('.menu-cache').is(':visible')) {
						$('.menu-btn').click();
					}
					scroll = 0;
				}
			});

	// FILTRE ÉLEVAGE --------------------------------------------------------------
		$(".filtre").click(function() {
			filtre=$(this).attr('id');
			if (filtre =='all') {
				$(".filtre").removeClass('active');
				$(this).addClass('active');
				$(".case").removeClass('cache');
			}else{
				$(".filtre").removeClass('active');
				$(this).addClass('active');
				$(".case").addClass('cache');
				$("." + $(this).attr('id')).removeClass('cache');
			}
		});

	// FULL CALENDAR -----------------------------------------------------------
		$('#calendar').fullCalendar({
			locale: $('#input-lang').val(),
			themeSystem: 'bootstrap4',
			fixedWeekCount : false,
			contentHeight : "auto",
			eventBackgroundColor : "white",
			eventSources :[
				{
					url : '/hebergement-json/'+$('#input-id').val(),
					type: 'POST'
				}
			],
			header:{
				left:'prev',
				center:'title',
				right:'next'
			}
		});
		$('#calendar2').fullCalendar({
			locale: $('#input-lang').val(),
			themeSystem: 'bootstrap4',
			fixedWeekCount : false,
			contentHeight : "auto",
			eventBackgroundColor : "white",
			eventSources :[
				{
					url : '/hebergement-json/3',
					type: 'POST'
				}
			],
			header:{
				left:'prev',
				center:'title',
				right:'next'
			}
		});
		$('#calendar3').fullCalendar({
			locale: $('#input-lang').val(),
			themeSystem: 'bootstrap4',
			fixedWeekCount : false,
			contentHeight : "auto",
			eventBackgroundColor : "white",
			eventSources :[
				{
					url : '/hebergement-json/4',
					type: 'POST'
				}
			],
			header:{
				left:'prev',
				center:'title',
				right:'next'
			}
		});

	//APERCU IMAGE ------------------------------------------------------------------------------------------
		$(".plus").click(function(){
			var url = $(this).attr('src');;
			$("body").append('<div class="fondapercu"><img class="visu" src='+url+'></div>');
			$(".fondapercu").hide(0,ouvrirapercu);
			fermerapercu();
		});
		function ouvrirapercu () {
			$(".fondapercu").fadeIn("slow");
		}
		function fermerapercu () {
			$(".fondapercu").click(function(){
				$(".fondapercu").fadeOut("slow",supprimer);
			});
			function supprimer () {
				$(".fondapercu").remove();
			}
		}
});