(function () {
	"use strict";

	var treeviewMenu = $('.app-menu');

	// Toggle Sidebar
	$('[data-toggle="sidebar"]').click(function(event) {
		event.preventDefault();
		$('.app').toggleClass('sidenav-toggled');
	});

	// Activate sidebar treeview toggle
	$("[data-toggle='treeview']").click(function(event) {
		event.preventDefault();
		if(!$(this).parent().hasClass('is-expanded')) {
			treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
		}
		$(this).parent().toggleClass('is-expanded');
	});

	// Set initial active toggle
	$("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

	//Activate bootstrip tooltips
	$("[data-toggle='tooltip']").tooltip();

})();

function selectTrad(id){
	$.post('/admin-traduction-selectTrad', {id : id}, function(data) {
		window.location.reload();
	});
}

$(".btn-navigation1").click(function(event) {
	$(".mosaique").removeClass('col-12');
	$(".mosaique").addClass('col-6');
});
$(".btn-navigation2").click(function(event) {
	$(".mosaique").removeClass('col-6');
	$(".mosaique").addClass('col-12');
});

// Ajout auto de l'étoile input required
$('input, select, textarea').each(function () {
	if (this.required) {
		$(this).closest('.form-group').find('label').append("<span class='text-danger'> *</span>");
	}
});